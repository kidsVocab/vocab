package com.example.vocabapp.vocabapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class TakePhoto extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_photo);


        ImageButton backButton = (ImageButton) findViewById(R.id.backFromTakePhoto);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(TakePhoto.this, Learnwords.class);
                startActivity(intent);
            }

        });

        ImageButton homeButton = (ImageButton) findViewById(R.id.homeFromTakePhoto);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(TakePhoto.this, Menu.class);
                startActivity(intent);
            }

        });
    }
}
