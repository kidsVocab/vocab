package com.example.vocabapp.vocabapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class Rhyme extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rhyme);

        ImageButton backButton = (ImageButton) findViewById(R.id.backFromRhyme);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Rhyme.this, Learnwords.class);
                startActivity(intent);
            }

        });

        ImageButton homeButton = (ImageButton) findViewById(R.id.homeFromRhyme);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Rhyme.this, Menu.class);
                startActivity(intent);
            }

        });


        Button saveButton = (Button) findViewById(R.id.saveButtonRhyme);
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView text = (TextView) findViewById(R.id.EditTextCategory);
                String val= text.getText().toString();
                System.out.println (val );
                Toast.makeText(Rhyme.this, "SAVED!", Toast.LENGTH_SHORT).show();

            }

        });


        //when the button is clicked get the text from the box and save it, produce a message saying that it has been saved.
    }
}
