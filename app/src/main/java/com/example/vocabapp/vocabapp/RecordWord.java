package com.example.vocabapp.vocabapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;


public class RecordWord extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_initial_sound);


        ImageButton backButton = (ImageButton) findViewById(R.id.backFromRecordInitialSound);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(RecordWord.this, Learnwords.class);
                startActivity(intent);
            }

        });
        ImageButton homeButton = (ImageButton) findViewById(R.id.homeFromRecordInitialSound);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(RecordWord.this, Menu.class);
                startActivity(intent);
            }

        });

    }
}

