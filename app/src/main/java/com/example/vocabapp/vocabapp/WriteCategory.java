package com.example.vocabapp.vocabapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Button;
import android.widget.TextView;
import android.view.View;
import android.content.Intent;
import	android.widget.Toast;




public class WriteCategory extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_category);

        ImageButton backButton = (ImageButton) findViewById(R.id.backFromWriteCategory);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(WriteCategory.this, Learnwords.class);
                startActivity(intent);
            }

        });

        ImageButton homeButton = (ImageButton) findViewById(R.id.homeFromWriteCategory);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(WriteCategory.this, Learnwords.class);
                startActivity(intent);
            }

        });

        //when the button is clicked get the text from the box and save it, produce a message saying that it has been saved.
        Button saveButton = (Button) findViewById(R.id.SaveButtonWriteCategory);
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView text = (TextView) findViewById(R.id.EditTextCategory);
                String val= text.getText().toString();
                System.out.println (val );
                Toast.makeText(WriteCategory.this, "SAVED!", Toast.LENGTH_SHORT).show();
            }

        });




    }

}
