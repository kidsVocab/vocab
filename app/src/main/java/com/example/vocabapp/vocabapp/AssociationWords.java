package com.example.vocabapp.vocabapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class AssociationWords extends AppCompatActivity {

    static int visible = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_association__words);

        ImageButton backButton = (ImageButton) findViewById(R.id.backFromAssociation);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(AssociationWords.this, Learnwords.class);
                startActivity(intent);
            }

        });

        ImageButton homeButton = (ImageButton) findViewById(R.id.homeFromAssociation);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(AssociationWords.this, Menu.class);
                startActivity(intent);
            }

        });

        Button saveButton = (Button) findViewById(R.id.saveButtonAssociation);
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView text = (TextView) findViewById(R.id.editTextAssociation);
                String val= text.getText().toString();
                System.out.println (val );
                //TODO save text to database, make sure the text stays in the textview box, or is loaded in on every page load
                Toast.makeText(AssociationWords.this, "SAVED!", Toast.LENGTH_SHORT).show();
                visible = 1;
            }

        });


    }

}
