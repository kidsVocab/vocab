package com.example.vocabapp.vocabapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


public class Learnwords extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learnwords);


        ImageButton cameraButton = (ImageButton) findViewById(R.id.camera);
        cameraButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Learnwords.this, TakePhoto.class);
                startActivity(intent);
            }

        });

        ImageButton megaphoneButton = (ImageButton) findViewById(R.id.megaphone);
        megaphoneButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Learnwords.this, RecordWord.class);
                startActivity(intent);
            }

        });

        ImageButton syllablesButton = (ImageButton) findViewById(R.id.syllables);
        syllablesButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Learnwords.this, Syllables.class);
                startActivity(intent);
            }

        });

        ImageButton sentenceButton = (ImageButton) findViewById(R.id.sentenceButton);
        sentenceButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Learnwords.this, Sentence.class);
                startActivity(intent);
            }

        });

        final ImageButton associationButton = (ImageButton) findViewById(R.id.thoughtBubble);
        associationButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Learnwords.this, AssociationWords.class);
                startActivity(intent);
            }

        });

        ImageButton RecordWordButton = (ImageButton) findViewById(R.id.megaphoneRecordWord);
        RecordWordButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Learnwords.this, RecordWord.class);
                startActivity(intent);
            }
        });

        ImageButton backButton = (ImageButton) findViewById(R.id.backFromLearnWords);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Learnwords.this, ChooseWord.class);
                startActivity(intent);
            }

        });

        ImageButton homeButton = (ImageButton) findViewById(R.id.homeFromLearnWords);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Learnwords.this, Menu.class);
                startActivity(intent);
            }

        });

        Button viewAllButton = (Button) findViewById(R.id.buttonViewControl);
        viewAllButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                associationButton.setVisibility(View.VISIBLE);
            }

        });




//if the association words page has been completed remove the button from learn words
        if (AssociationWords.visible == 1){
            associationButton.setVisibility(View.GONE);
        }
    }





}
