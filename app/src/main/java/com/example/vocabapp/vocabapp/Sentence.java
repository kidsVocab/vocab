package com.example.vocabapp.vocabapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class Sentence extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sentence);

        ImageButton backButton = (ImageButton) findViewById(R.id.backFromSentence);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Sentence.this, Learnwords.class);
                startActivity(intent);
            }

        });

        ImageButton homeButton = (ImageButton) findViewById(R.id.homeFromSentence);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Sentence.this, Menu.class);
                startActivity(intent);
            }

        });
    }
}
