package com.example.vocabapp.vocabapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class ChooseWord extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_word);

        ImageButton backButton = (ImageButton) findViewById(R.id.backFromChooseWord);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ChooseWord.this, ChooseTopic.class);
                startActivity(intent);
            }

        });

        ImageButton homeButton = (ImageButton) findViewById(R.id.homeFromChooseWord);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ChooseWord.this, Menu.class);
                startActivity(intent);
            }

        });

        ImageButton tempButton = (ImageButton) findViewById(R.id.tempbuttonChooseWord);
        tempButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ChooseWord.this, Learnwords.class);
                startActivity(intent);
            }

        });
    }

}
