package com.example.vocabapp.vocabapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Button;

public class ChooseTopic extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_topic);


        ImageButton backButton = (ImageButton) findViewById(R.id.backFromChooseTopic);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ChooseTopic.this, Menu.class);
                startActivity(intent);
            }

        });

        ImageButton homeButton = (ImageButton) findViewById(R.id.homeFromChooseTopic);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ChooseTopic.this, Menu.class);
                startActivity(intent);
            }

        });

    //TODO Change the temp button to be a button with all the topic choices in from the database
        ImageButton tempButton = (ImageButton) findViewById(R.id.tempbutton);
        tempButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ChooseTopic.this, ChooseWord.class);
                startActivity(intent);
            }

        });


    }


}
